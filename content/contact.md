+++
title = "Contact"
id = "contact"
+++

From Thrissur,
* Get into a local private bus to Kunnamkulam (You can easily hop in one near the railway station itself).
* Get off at the bus stop named "Kaipparambu" `കൈപ്പറമ്പ്`
* It's 2kms from there to college. Use an auto (50Rs max)

From Kunnamkulam
* Get into a local private bus to Thrissur
* Get off at the bus stop named "Kaipparambu" `കൈപ്പറമ്പ്`
* It's 2kms from there to college. Use an auto (50Rs max)

From Kozhikode,
* Reach Kunnamkulam via KSRTC or private bus
